# IDS-721-Cloud-Computing :computer:

## Mini Project 7 :page_facing_up: 

## :ballot_box_with_check: Requirements
* Ingest data into Vector database
* Perform queries and aggregations
* Visualize output

## :ballot_box_with_check: Grading Criteria
* __Data ingestion__ 30%
* __Query functionality__ 30%
* __Visualizations__ 30%
* __Documentation__ 10%

## :ballot_box_with_check: Deliverables
* Rust code
* Screenshots or demo video showing successful invocation
* Writeup explaining service

## :ballot_box_with_check: Main Progress
* `qdrant`: The project utilizes Qdrant, a vector database, for data ingestion, specifically inserting and storing random vectors along with their statistical metadata.
```bash
λ docker run -p 6334:6334 qdrant/qdrant
```
![Qdrant](https://github.com/suim-park/Machine-Learning-Assignment-1/assets/143478016/79342efa-ea16-4fef-84c2-b7509a1560f6)

1. __`Data Ingestion into Vector Database`__: The data ingestion into the vector database (Qdrant) occurs in the `insert_vector_data` function. This function iterates over each vector's statistics, packages them into a payload, and inserts each vector along with its payload into the Qdrant collection specified by `collection_name`. Here's a quick overview:
```rust
async fn insert_vector_data(client: &QdrantClient, collection_name: &str, vectors: &HashMap<u64, VectorStatistics>) -> Result<()> {
    for (index, stats) in vectors.iter() {
        let payload = json!({
            "min": stats.min,
            "q1": stats.q1,
            "median": stats.median,
            "q3": stats.q3,
            "max": stats.max
        });

        let payload: Payload = payload.try_into().map_err(|e| anyhow!("Payload conversion error: {:?}", e))?;
        let vector = generate_random_vector(10); // Generates a random vector for each set of statistics
        client.upsert_points_blocking(collection_name, None, vec![PointStruct::new(*index, vector, payload)], None).await?;
    }

    Ok(())
}
```

2. __`Perform queries and aggregations`__: The part of the code responsible for performing queries (specifically searching for vectors based on similarity to a query vector) is found in the `search_and_display_results` function. This function sends a search query to the Qdrant database, requesting the closest vectors to a specified query vector, and then prints out the search results, including point IDs and their payloads. However, this code snippet doesn't perform aggregations; it focuses solely on vector search operations.

Here's the relevant section:
```rust
async fn search_and_display_results(client: &QdrantClient, collection_name: &str) -> Result<()> {
    let query_vector: Vec<f32> = vec![0.5; 10]; // Adjust as needed

    let search_result = client.search_points(&SearchPoints {
        collection_name: collection_name.into(),
        vector: query_vector,
        filter: None,
        limit: 5,
        with_payload: Some(true.into()),
        ..Default::default()
    }).await?;

    println!("Search Results:");
    for (index, point) in search_result.result.iter().enumerate() {
        println!("Point {}: ID = {:?}, Payload = {:?}", index + 1, point.id, point.payload);
    }

    Ok(())
}
```

3. __`Visualize Output`__: The visualization of output in the provided code occurs in the println! statements within the `search_and_display_results` function. This function prints the search results to the console, displaying each found point's ID and its associated payload data. While this approach offers a basic form of output visualization by listing the results in the terminal, it does not employ graphical visualization techniques.

Here's the specific part of the code responsible for output visualization:

```rust
println!("Search Results:");
for (index, point) in search_result.result.iter().enumerate() {
    println!("Point {}: ID = {:?}, Payload = {:?}", index + 1, point.id, point.payload);
}
```

* Visiual Outputs<br/>
![Statistics Results](https://github.com/suim-park/Machine-Learning-Assignment-1/assets/143478016/c03991be-97f4-4f83-b143-2dfeaad91606)